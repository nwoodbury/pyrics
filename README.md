# pyrics #

The `pyrics` library is intended to become a comprehensive Python library for the representation, identification, and control of dynamic systems. It began its life as a part of [2].

## Installation ##

This library is developed and tested in Python 3.6.7. It is not intended to be
backwards compatible with Python 2. As such, it is **highly** recommended that
all these steps be done within a virtual environment configured with Python 3
(`virtualenvwrapper` is also recommended to create and manage these virtual
environments).

We also assume that this library is being installed in an Ubuntu
environment, though it should also work in Windows or OS X environments as well
with minor variations to the installation process.

Install core project dependencies with:

    pip install -U numpy pandas matplotlib jupyter

Navigate to the project root and install `pyrics` with:

    python setup.py install

or (if you intend to modify and contribute to `pyrics`):

    python setup.py develop

Also, if contributing, install `jq`, which is used for using git to automate the
cleaning of jupyter notebooks before committing.

    sudo apt-get install jq

Then, while in the project root, run the command:

    >> git config --local include.path ../.gitconfig

## Testing ##

Unit tests can be run with

    python setup.py test

Alternatively, they can be run through the script `scripts\run_tests.py`, which
is `Hydrogen` compatible. If this script is run from outside `Atom`, then it
should be run from the root project directory as:

    python scripts/run_tests.py

## Development Style Guide ##

All style when developing should pass `pylint`. Definitions are provided in
`pylintrc` located in the project root. One notable deviation in style from
pep8 is that indentations should be defined as **2 spaces**, not the standard 4.

## Licensing ##

This code is © Nathan Woodbury, 2019, and it is made available under the Apache 2.0 license enclosed with the software.

Over and above the legal restrictions imposed by this license, if you use this software for an academic publication then you are obliged to provide proper attribution. This can be to this code directly,

    [1] N. Woodbury. pyrics, v1.0 (2019). gitlab.com/idealabs/netreco

or to the paper that describes it

    [2] N. Woodbury, "Representation and Reconstruction of Linear, Time-Invariant Networks," Ph.D Dissertation, Brigham Young Univ., Provo, UT, 2019.
