import numpy as np
import sympy as sp
from sympy import latex
from pyrics.Representations import DTSS
# from sympy.abc import z


A = np.array([
    [0, 1],
    [-2, -3]
])
B = np.array([[0], [1]])
C = np.array([[1, 0]])

ss = DTSS(A, B, C)

# To TF
G = ss.to_TF()
sp.init_printing(use_latex='mathjax')
G
# print(G.latex())
# print(G.limit())
G.G.inv()
G.G ** 2
