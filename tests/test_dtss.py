"""Tests Representations\DTSS.py
"""
import pytest
import numpy as np

from pyrics.Representations import DTSS


# 000000000000000000000000000000000000000000000000000000000000000000000000000000
# Initialization Tests
# 000000000000000000000000000000000000000000000000000000000000000000000000000000

class TestInitialization():
    """Tests the initialization of the discrete time state space models,
    making sure dimensions are properly aligned.
    """

    def test_aligned_simple(self):
        """Tests to make sure that the SS is successfully initialized when
        dimensions properly align.
        """
        success = False
        try:
            A = np.zeros((3, 3))
            B = np.zeros((3, 3))
            C = np.zeros((3, 3))

            ss = DTSS(A, B, C)

            assert ss.n == 3
            assert ss.m == 3
            assert ss.p == 3

            success = True

        except Exception as e:
            print(e)
            pass

        assert success

    def test_aligned_D(self):
        """Tests to make sure that the SS is successfully initialized when
        dimensions properly align.
        """
        success = False
        try:
            A = np.zeros((3, 3))
            B = np.zeros((3, 3))
            C = np.zeros((3, 3))
            D = np.zeros((3, 3))

            ss = DTSS(A, B, C, D)

            assert ss.n == 3
            assert ss.m == 3
            assert ss.p == 3

            success = True

        except Exception as e:
            print(e)
            pass

        assert success

    def test_aligned_dims(self):
        """Tests to make sure that the SS is successfully initialized when
        dimensions properly align. Makes sure that dimensions are properly
        recorded as well.
        """
        success = False
        try:
            A = np.zeros((2, 2))
            B = np.zeros((2, 3))
            C = np.zeros((4, 2))
            D = np.zeros((4, 3))

            ss = DTSS(A, B, C, D)

            assert ss.n == 2
            assert ss.m == 3
            assert ss.p == 4

            success = True

        except Exception as e:
            print(e)
            pass

        assert success

    def test_A_square(self):
        """Tests to make sure A is square.
        """
        A = np.zeros((3, 2))
        B = np.zeros((3, 3))
        C = np.zeros((3, 3))

        with pytest.raises(ValueError):
            DTSS(A, B, C)

    def test_bad_align_AB(self):
        """Exception should be raised when A and B do not have the same number
        of rows.
        """
        A = np.zeros((3, 3))
        B = np.zeros((2, 2))
        C = np.zeros((3, 3))

        with pytest.raises(ValueError):
            DTSS(A, B, C)

    def test_bad_align_AC(self):
        """Exception should be raised when A and C do not have the same number
        of columns.
        """
        A = np.zeros((3, 3))
        B = np.zeros((3, 3))
        C = np.zeros((2, 2))

        with pytest.raises(ValueError):
            DTSS(A, B, C)

    def test_bad_align_BD(self):
        """Exception should be raised when B and D do not have the same number
        of columns.
        """
        A = np.zeros((3, 3))
        B = np.zeros((3, 3))
        C = np.zeros((3, 3))
        D = np.zeros((3, 2))

        with pytest.raises(ValueError):
            DTSS(A, B, C, D)

    def test_bad_align_CD(self):
        """Exception should be raised when C and D do not have the same number
        of rows.
        """
        A = np.zeros((3, 3))
        B = np.zeros((3, 3))
        C = np.zeros((3, 3))
        D = np.zeros((2, 3))

        with pytest.raises(ValueError):
            DTSS(A, B, C, D)
