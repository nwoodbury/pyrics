#------------------------------------------------------------
# pyrics/PolynomialUtilities.py
#
# Collection of miscellaneous utilities related to sympy polynomials.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from sympy import degree, diff, sympify, fraction

#-------------------------------------------------------------
def is_polynomial(expr, arg):
  '''Determine whether `expr` is a polynomial in `arg`.

  Parameters
  ----------
  expr : sympy expression (or something that can be cast as one)

  Returns
  -------
  is_polynomial : bool
  '''
  expr = sympify(expr)
  if expr.is_real:
    # Base case: given a number
    return True

  d = degree(expr)
  fin = expr
  for _ in range(d):
    fin = diff(fin, arg)

  return fin.is_real

#------------------------------------------------------------
def is_rational_polynomial(expr, arg):
  '''Determine whether `expr` is a rational polynomial in `arg`.

  Parameters
  ----------
  expr : sympy expression (or something that can be cast as one)

  Returns
  -------
  is_rational_polynomial : bool
  '''
  expr = sympify(expr)
  n, d = fraction(expr)
  return is_polynomial(n, arg) and is_polynomial(d, arg)

#------------------------------------------------------------
def is_proper(expr, arg):
  '''Determine if the given expression is a proper polynomial in arg.

  Parameters
  ----------
  expr : sympy expression
    If not a rational polynomial (see `is_rational_polynomial()`, raises
    `ValueError`)

  Returns
  -------
  is_proper : bool
  '''
  expr = sympify(expr)
  if not is_rational_polynomial(expr, arg):
    raise ValueError(
      'Can only check properness of rational polynomials.'
    )
  n, d = fraction(expr)
  return _degree(d) >= _degree(n)

#------------------------------------------------------------
def is_strictly_proper(expr, arg):
  '''Determine if the given expression is a strictly proper polynomial in arg.

  Parameters
  ----------
  expr : sympy expression\linespread{0.2}
    If not a rational polynomial (see `is_rational_polynomial()`, raises
    `ValueError`)

  Returns
  -------
  is_proper : bool
  '''
  expr = sympify(expr)
  if not is_rational_polynomial(expr, arg):
    raise ValueError(
      'Can only check properness of rational polynomials.'
    )
  if expr == 0:
    # Special case, 0 is considered to be strictly proper
    return True
  n, d = fraction(expr)
  return _degree(d) > _degree(n)

#------------------------------------------------------------
def _degree(p):
  '''Wrapper around sympy's `degree()`, setting the degree of numbers to be
  0.
  '''
  if p.is_real:
    return 0
  return degree(p)
