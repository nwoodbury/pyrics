#------------------------------------------------------------
# pyrics/utilities.py
#
# A collection of miscellaneous utilities used within the framework.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from datetime import datetime
from sympy import latex
from IPython.display import display, Math

#------------------------------------------------------------
def list_diff(left, right):
  '''Treat each list as a set and return the set difference left - right.

  Parameters
  ----------
  left : list
  right : list

  Returns
  -------
  diff : list
  '''
  return list(set(left) - set(right))

#------------------------------------------------------------
def pprint(m):
  '''Return a pretty (latex) formatted version of the math in m for use in
  a Jupyter notebook.
  '''
  return display(Math(latex(m)))

#------------------------------------------------------------
def vprint(msg, verbose):
  '''A verbose printing utility.

  If verbose, displays "[time] : [msg]"

  Parameters
  ----------
  msg : str
    The message to print
  verbose : bool
    If False, don't print anything.
  '''
  if verbose:
    print(datetime.now(), ':', msg)
