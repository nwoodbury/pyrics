#------------------------------------------------------------
# pyrics/Plot/PlotPassiveComparison.py
#
# Plotting utilities to compare time-domain reconstructed and actual models.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from math import ceil
import plotly.graph_objs as go


def plotTimeReconstructionComparison(plot, QPimp, QPconv=None, QPact=None,
                                     ylimbound=0.1):
  '''Generate plots (one for each impulse response) comparing the response in
  QPimp to that in QPconv and QPact (if given).

  This is designed such that QPimp is the response found through the least
  squares portion of the passive reconstruction algorith, QPconv is QPimp
  converted into convolutional form and QPact, if given, is the impulse response
  from the actual model that generated the data.

  Parameters
  ----------
  plot : function (plotly plot function)
    The plotly plotting function
  QPimp : ImpulseDSF
  QPconv : ConvolutionalDSF
  QPact : DiscreteDSF
  ylimbound : number > 0
    Prevents plotly from zooming in too far and making negligibly-zero impulse
    responses look non-zero.
    If there are any values less than 0, then the plot will begin at y no
    greater than ``-ylimbound` on the negative side of the y axis (otherwise,
    the plot can start at 0 on the y axis). If there are any positive values
    greater than zero, then the plot will end at y no less than `ylimbound`
    (otherwise, the plot can end at 0 on the y axis). If there are no non-zero
    values, then the y axis will range from `-ylimbound` to `ylimbound`
  '''
  p, m = QPimp.shape
  r = QPimp.r

  comma = ''
  if max(p, m) >= 10:
    comma = ','

  if QPconv is not None:
    p1, m1 = QPconv.shape
    assert p == p1
    assert m == m1

    QPconvimp = QPconv.to_impulse(r=r)
    QPactimp = None

  if QPact is not None:
    p2, m2 = QPact.shape
    assert p == p2
    assert m == m2

    QPactimp = QPact.to_convolutional().to_impulse(r=r)

  def getQorP(QorP, QP):
    assert QorP in ['Q', 'P']

    if QorP == 'Q':
      return QP.Q

    return QP.P

  def plotImp(QorP, i, j):
    traces = []

    if QPactimp is not None:
      traces.append(go.Scatter(
        x=list(range(r)),
        y=getQorP(QorP, QPactimp)[i, j].impulse,
        name='Actual Model',
        line=dict(
          width=2,
          dash='dashdot'
        )
      ))

    traces.append(go.Scatter(
      x=list(range(r)),
      y=getQorP(QorP, QPimp)[i, j].impulse,
      name='Reconstructed Impulse Response',
      mode='markers'
    ))

    if QPconv is not None:
      traces.append(go.Scatter(
        x=list(range(r)),
        y=getQorP(QorP, QPconvimp)[i, j].impulse,
        name='Projected Convolutional Form',
        mode='lines'
      ))

    name = r'${}_{{{}{}{}}}$'.format(QorP, i + 1, comma, j + 1)

    def ylimoneside(mx):
      if mx <= 0:
        return 0
      return max(ceil((mx + 0.01) * 10) / 10, ylimbound)

    def yf(fcn):
      mn = fcn(getQorP(QorP, QPimp)[i, j].impulse)
      if QPact is not None:
        mnact = fcn(getQorP(QorP, QPactimp)[i, j].impulse)
        mn = fcn(mnact, mn)
      if QPconv is not None:
        mnconv = fcn(getQorP(QorP, QPconvimp)[i, j].impulse)
        mn = fcn(mnconv, mn)

      return mn

    def ylim():
      lb = -ylimoneside(-yf(min))
      ub = ylimoneside(yf(max))

      if lb == 0 and ub == 0:
        return [-ylimbound, ylimbound]
      return lb, ub

    layout = go.Layout(
      title='{} Comparisons'.format(name),
      xaxis=dict(title=r'$t = 0, \ldots, r$'),
      yaxis=dict(title='{}(t)'.format(name), range=ylim()),
    )

    plot(dict(data=traces, layout=layout))

  # Plot impulse responses in Q
  for i in range(p):
    for j in range(p):
      plotImp('Q', i, j)

  # Plot impulse responses in P
  for i in range(p):
    for j in range(m):
      plotImp('P', i, j)
