#------------------------------------------------------------
# pyrics/Algorithms/identifiability.py
#
# Verifies and encodes identifiability conditions.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

import numpy as np
from numpy.linalg import pinv, matrix_rank as rank

def checkIdentifiability(K, p, m):
  '''Ensure that the indentifiability conditions are appropriate, or create them
  if not given (assumes target specificity with hollow Q if not given).

  The DSF being reconstructed has Q as p x p and P as p x m.

  Raises
  ------
  ValueError if K does not appropriately encode indentifiability conditions.
  This is triggered if:
    - K is not (p^2 + pm x pm)
    - K is not full column rank (i.e., rank(K) != pm)

  Parameters
  ----------
  K : None or np.array (p^2 + pm x k)
    The encoding of the identifiability conditions, or None if target
    specificity is to be encoded. Must have k <= pm and rank K = k.
  p : int > 0
  m : int > 0

  Returns
  -------
  K : (p^2 + pm x pm)
    A copy of K if K was not None, or the newly created K if it was none.
  Ki : (pm x p^2 + pm)
    The Moore-penrose pseudo inverse of K (i.e., Ki * K = I_pm, where I_pm is
    the pm x pm identity matrix).
  '''
  p2 = p * p
  pm = p * m
  p2pm = p2 + pm

  if K is None:
    if p != m:
      raise ValueError((
        'If K is not given, must have p = m (P square) in order to encode '
        'target specificity. Given p = {} and m = {}'
      ).format(p, m))
    K11 = np.eye(p2)
    K22 = np.eye(pm)
    Qkeep = []
    Pkeep = []
    ix = 0
    for i in range(p):
      for j in range(m):
        if i == j:
          Pkeep.append(ix)
        else:
          Qkeep.append(ix)
        ix += 1
    K11 = K11[:, Qkeep]
    K22 = K22[:, Pkeep]
    K12 = np.zeros((p2, m))
    K21 = np.zeros((pm, p2 - p))
    Ktop = np.concatenate((K11, K12), axis=1)
    Kbot = np.concatenate((K21, K22), axis=1)
    K = np.concatenate((Ktop, Kbot), axis=0)

  rows, cols = K.shape

  if rows != p2pm:
    raise ValueError((
      'K must have p^2 + pm rows. Given p^2 + pm = {}, but K has {} rows'
    ).format(p2pm, rows))
  if cols > pm:
    raise ValueError((
      'K must have pm or fewer columns. Given pm = {}, but K has {} columns'
    ).format(pm, cols))

  rK = rank(K)

  if rK != cols:
    raise ValueError((
      'K must be full-column rank (i.e., rank(K) must be k = {}), '
      'but rank(K) = {}'
    ).format(cols, rK))

  return K, pinv(K)
