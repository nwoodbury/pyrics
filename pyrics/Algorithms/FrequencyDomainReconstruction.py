#------------------------------------------------------------
# pyrics/Algorithms/FrequencyDomainReconstruction.py
#
# The frequency-domain reconstruction algorithm.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from sympy import factor
from sympy.matrices import Matrix

from pyrics.Algorithms.Identifiability import checkIdentifiability

#------------------------------------------------------------
def frequencyReconstruct(G, K=None):
  '''Reconstruct the network (Q, P) generating G.

  Note that G only needs to be a sympy matrix, and note that Q, P will also be
  sympy matrices.

  Methodology described in [1, 2], following the notation of [2].

  Attributes
  ----------
  (these are available at `active_reconstruct.attribute` and can be used to
  view and access the intermediate computations)

  Munstacked : sympy matrix (p x [p + m])
    The augmented matrix [G' I].
  Mstacked : sympy matrix (pm x [p^2 + pm])
    Munstacked that has been converted into stacked form (after vectorizing)
    the unknowns in Q and P
  M : sympy matrix (pm x pm)
    Mstacked after the identifiability conditions have been applied to reduce
    the number of columns.
  y : sympy matrix (pm x 1)
    The vector containing the known elements from G, stacked.
  x : sympy matrix (pm x 1)
    The vector containing the reconstructed elements from Q and P, stacked,
    where x = M^-1 y.

  Parameters
  ----------
  G : sympy matrix (p x m)
    The 'transfer function'
  K : np.array (p^2 + pm x pm) or None, default=None
    The identifiability conditions required to map the TF to the DSF uniquely.
    If None, assumes that P is diagonal (target specificity); however, an
    exception is raised in this case if p != m.
  display_intermediate : bool, default=False
    If true, prints the intermediate steps, including M (unstacked),
    M (stacked), y, x, Mhat, and xhat.

  Returns
  -------
  Q : sympy matrix (p x p)
  P : sympy matrix (p x m)

  Sources
  -------
  [1] J. Gonçalves and S. Warnick, "Necessary and Sufficient Conditions
      for Dynamical Structure Reconstruction of LTI Networks," IEEE
      Transactions on Automatic Control, Aug. 2008.
  [2] N. Woodbury, "Representation and Reconstruction of Linear, Time-Invariant
      Networks," Ph.D Dissertation, Brigham Young Univ., Provo, UT, 2019.
  '''
  self = frequencyReconstruct
  p, m = G.shape

  K, _ = checkIdentifiability(K, p, m)
  _, k = K.shape

  Lunstacked = G.transpose().row_join(Matrix.eye(m))
  LQ = Matrix.zeros(p * m, p ** 2)
  GT = G.transpose()
  for i in range(p):
    start_row = i * m
    end_row = (i + 1) * m
    start_col = i * p
    end_col = (i + 1) * p
    LQ[start_row: end_row, start_col: end_col] = GT

  L = LQ.row_join(Matrix.eye(p * m))
  M = L * K

  gvec = Matrix.zeros((p * m), 1)
  for i in range(p):
    start = m * i
    end = m * (i + 1)
    gvec[start: end, 0] = G[i, :].transpose()

  if k == p * m:
    # from pyrics.utilities import pprint
    # pprint(M)
    thetahat = M.inv() * gvec
  else:
    thetahat = factor((factor(M.transpose() * M)).inv()) * M.transpose() * gvec

  thetahat = factor(thetahat)
  theta = factor(K * thetahat)

  Q = Matrix.zeros(p, p)
  P = Matrix.zeros(p, m)
  xix = 0
  for i in range(p):
    for j in range(p):
      Q[i, j] = theta[xix, 0]
      xix += 1

  for i in range(p):
    for j in range(m):
      P[i, j] = theta[xix, 0]
      xix += 1

  self.Lunstacked = Lunstacked
  self.L = L
  self.M = factor(M)
  self.gvec = gvec
  self.thetahat = factor(thetahat)
  self.K = K
  self.theta = theta

  return Q, P
