#------------------------------------------------------------
# pyrics/Representations/ImpulseTF.py
#
# Representation of a transfer function in impulse response form.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from functools import partial
from multiprocessing import Pool

import numpy as np
from plotly import tools

from pyrics.utilities import vprint as vprintfull
from .SystemRepresentation import SystemRepresentation
from .ImpulseScalar import ImpulseScalar


################################################################################
def _convolutionalConvert(job, verbose, order, bounds, tol, name):
  '''Utility used in converting to a convolutional form.

  Defined outside the class for paralellization.
  '''
  vprint = partial(vprintfull, verbose=verbose)

  i, j, scalar = job
  vprint('Converting {}[{}, {}]'.format(name, i + 1, j + 1))
  conv = scalar.to_convolutional(order=order, bounds=bounds, tol=tol)
  vprint('{}[{}, {}] finished'.format(name, i + 1, j + 1))
  return i, j, conv

################################################################################
class ImpulseTF(SystemRepresentation):
  '''Representation of a transfer function matrix in impulse form.

  Parameters
  ----------
  G : list of lists of ImpulseScalar (p x m array)
  '''

  #------------------------------------------------------------
  def __init__(self, G):
    self.G = G
    self.p = len(G)
    assert self.p > 0
    self.m = len(G[0])

    r = None
    rij = (-1, -1)
    for i, inner in enumerate(self.G):
      assert len(inner) == self.m
      for j, el in enumerate(inner):

        assert isinstance(el, ImpulseScalar)

        # Check that every r is the same
        # To consider: find the max r and replace every element with an
        #   augmented impulse response of that length.
        if r is None:
          r = el.r
          rij = (i, j)
        else:
          if el.r != r:
            raise ValueError((
              'Every impulse response must have the same length `r`. We have '
              'G[{}, {}].r = {} but G[{}, {}].r = {}'
            ).format(rij[0], rij[i], r, i, j, el.r))

    self.r = r

  #------------------------------------------------------------
  @property
  def shape(self):
    '''Give the shape of this convlolutional TF, which is p x m.

    Returns
    -------
    shape : (int > 0, int > 0)
    '''
    return (self.p, self.m)

  #####################################################################
  #   Public functions
  #####################################################################

  #------------------------------------------------------------
  def scalar_one_norm(self):
    '''Compute a matrix of the same dimensions as this, where the entries
    are the one-norms of the corresponding impulse response.

    Returns
    -------
    M : np.array (p x m)
    '''
    M = np.zeros((self.p, self.m))
    for i in range(self.p):
      for j in range(self.m):
        M[i, j] = self[i, j].one_norm()

    return M

  #####################################################################
  #   Conversion functions
  #####################################################################

  def to_convolutional(self, order=3, bounds=10, verbose=False, tol=1e-3,
                       njobs=None, name='G'):
    '''Convert this ImpulseTF into a ConvolutionalTF.

    Parameters
    ----------
    order : int > 0
      The number of poles (`w`) to add in the convolutional form, meaning the
      convolutional form will have `2 * order + 1` parameters.
    bounds : int > 0
      All parameters in the convolutional form will be bounded to be between
      -bounds and bounds.
    verbose : bool, default=False
      If true, prints status messages.
    tol : number > 0
      If the one-norm of any impulse response is less than tol, then 0 will
      be returned instead of attempting to convert to a convolutional scalar.
    njobs : int > 0 or None, default=None
      If not None, paralellizes the conversion of each scalar across a different
      job.
    name : str, default='G'
      The name to display for this matrix in the status messages if verbose
      is True.

    Returns
    -------
    G : ConvolutionalTF
    '''
    from .ConvolutionalTF import ConvolutionalTF

    # Define the job function
    func = partial(
      _convolutionalConvert, verbose=verbose, order=order, bounds=bounds,
      tol=tol, name=name
    )

    # Build the list of jobs
    jobs = []
    for i in range(self.p):
      for j in range(self.m):
        jobs.append((i, j, self[i, j]))

    # Run the jobs
    if njobs is None:
      mapfunc = map
    else:
      p = Pool(njobs)
      mapfunc = p.map
    rs = mapfunc(func, jobs)

    # Build a placeholder for the results
    G = []
    for i in range(self.p):
      G.append([])
      for j in range(self.m):
        G[i].append(None)

    # Extract the results
    for rsi in rs:
      i, j, scalar = rsi
      G[i][j] = scalar

    return ConvolutionalTF(G)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    return repr(self.G)

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this transfer function.

    kwargs
    ----------
    name : str or None, default = G
      Prefixes the latex with 'name = ' for whatever name is given. If
      name is None, just gives the equation.
    length : int > 0, default=3
      Truncates the impulse response representation to the first `length` items.

    Returns
    -------
    latex : str
    '''
    name = kwargs.get('name', 'G(t)')

    # sp.init_printing(use_latex='mathjax')
    if name is not None:
      prefix = '{} = '.format(name)
    else:
      prefix = ''

    matrixstr = '\\\\'.join(
      [' & '.join(
          [el.latex(name=None, length=kwargs.get('length', 3)) for el in inner]
        ) for inner in self.G
      ]
    )
    return '{} \\begin{{bmatrix}}{}\\end{{bmatrix}}'.format(prefix, matrixstr)

  #------------------------------------------------------------
  def get_plotly(self, name='f(t)', plot=None, title='', scale=200, **kwargs):
    '''Generate and return plotly data for a time-series representation of
    this matrix of impulse responses. If `plot`, also draws a plotly figure.

    Parameters
    ----------
    name : str, default='G(t)'
      The prefix to assign to each time series and plot title.
    plot : function (plotly plot function) or None, default=None
      If None, only generates and returns the data. If a function is given,
      plots a single plot of the impulse responses in G using that function.
    title : str, default=''
      Only used if plot is not None. The title to assign to the plot.
    scale : int > 0
      The dimensions of the figure will be p * scale x p * scale * 1.5

    kwargs
    ------
    Any additional parameter that can be used by `go.Scatter`. These will all be
    passed in to every scalar impulse responses plot function.

    Returns
    -------
    traces : dict (tuple -> plotly.graph_objs)
      Maps (i, j) for the impulse response G_ij(t) to the plot trace.
    '''
    traces = {}

    if max(self.p, self.m) < 10:
      comma = ''
    else:
      comma = ','

    subplot_titles = []
    for i in range(self.p):
      for j in range(self.m):
        if '(t)' in name:
          left, part, right = name.partition('(t)')
          label = r'${}_{{{}{}{}}}{}{}$'.format(
            left, i + 1, comma, j + 1, part, right
          )
        else:
          label = r'${}_{{{}{}{}}}$'.format(name, i + 1, comma, j + 1)

        trace = self[i, j].get_plotly(name='', **kwargs)
        traces[(i, j)] = trace
        subplot_titles.append(label)

    if plot is not None:
      fig = tools.make_subplots(
        rows=self.p, cols=self.m, subplot_titles=subplot_titles,
        print_grid=False
      )

      for ix, trace in traces.items():
        i = ix[0] + 1
        j = ix[1] + 1
        fig.append_trace(trace, i, j)

      fig['layout'].update(
        title=title,
        height=self.p * scale,
        width=self.m * scale * 1.5,
        showlegend=False
        # xaxis=dict(title='$r$'),
        # yaxis=dict(title='${}$'.format(name))
      )
      plot(fig)

    return traces

  #####################################################################
  #   Magic Functions
  #####################################################################

  #------------------------------------------------------------
  def __getitem__(self, key):
    assert len(key) == 2
    return self.G[key[0]][key[1]]

  #------------------------------------------------------------
  def __setitem__(self, key, value):
    assert len(key) == 2
    self.G[key[0]][key[1]] = value
