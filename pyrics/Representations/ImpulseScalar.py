#------------------------------------------------------------
# pyrics/Representations/ImpulseScalar.py
#
# Representation of a rational function in impulse response form.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

import numpy as np
import plotly.graph_objs as go
from scipy.optimize import differential_evolution as evolution

from .SystemRepresentation import SystemRepresentation


class ImpulseScalar(SystemRepresentation):
  '''The representation of a linear finite impulse response; i.e.,

      f(t) = [f(0), f(1), f(2), ..., f(r - 1)]

  for some integer r > 0, and where G(t) = 0 for t < 0 and t >= r.

  Parameters
  ----------
  impulse : list (length r)
    The impulse response, where the item at index i is f(i).
  precision : number > 0
    f(t) such that |f(t)| < tol will be set to zero.
  '''

  #------------------------------------------------------------
  def __init__(self, impulse, precision=1e-6):
    impulse = list(impulse)
    self.r = len(impulse)
    self.precision = precision

    self.impulse = []
    for ft in impulse:
      try:
        val = float(ft)
        if abs(val) < precision:
          val = 0
        self.impulse.append(val)
      except:
        raise ValueError('impulse must be a list of numbers')

  #####################################################################
  #   Public functions
  #####################################################################

  #------------------------------------------------------------
  def one_norm(self):
    '''Get the one-norm of this impulse response.

    The one-norm is the sum of the absolute values of every entry in the impulse
    response.

    Returns
    -------
    norm : float >= 0
    '''
    return np.linalg.norm(self.impulse, ord=1)

  #####################################################################
  #   Conversion functions
  #####################################################################

  #------------------------------------------------------------
  def to_convolutional(self, order=3, bounds=10, tol=1e-3, normord=1):
    '''Project this impulse response to the nearest convolutional form of order
    `order`.

    Parameters
    ----------
    order : int > 0
      The number of poles (`w`) to add in the convolutional form, meaning the
      convolutional form will have `2 * order + 1` parameters.
    bounds : int > 0
      Parameters a and b in the convolutional form will be bounded to be between
      -bounds and bounds. Parameter c will always be between -1 and 1
      (exclusive) so that the impulse response is stable.
    tol : number > 0
      If the `normord`-norm of this impulse response is less than tol, then a 0
      convolutional scalar will be returned without attempting to find a fit.
    normord : {int > 0, inf, -inf, 'fro', 'nuc'}, default=1
      The norm to use. Equivalent to the parameter `ord` in `np.linalg.norm()`.

    Returns
    -------
    ft : ConvolutionalScalar
    '''
    from .ConvolutionalScalar import ConvolutionalScalar

    if np.linalg.norm(self.impulse, ord=normord) < tol:
      return ConvolutionalScalar([0])

    def func(x):
      conv = ConvolutionalScalar(x)
      ximp = conv.to_impulse(r=self.r)
      dist = np.linalg.norm(
        np.array(self.impulse) - np.array(ximp.impulse), ord=normord
      )
      return dist

    boundslist = [(-bounds, bounds)]
    for _ in range(order):
      boundslist += [(-bounds, bounds), (-1 + 1e-6, 1 - 1e-6)]

    res = evolution(func, boundslist)
    return ConvolutionalScalar(res.x)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def get_plotly(self, name='f(t)', plot=None, title='', **kwargs):
    '''Generate and return plotly data for a time-series representation of
    this impulse response. If `plot`, also draws a plotly figure.

    Parameters
    ----------
    name : str, default='f(t)'
      The name to assign to the time series. Useful if multiple series are added
      to the same plot, this gives each a label in the legend.
    plot : function (plotly plot function) or None, default=None
      If None, only generates and returns the data. If a function is given,
      plots a single plot of this impulse response using that function.
    title : str, default=''
      Only used if plot = True. The title to assign to the plot.

    kwargs
    ------
    Any additional parameter that can be used by `go.Scatter`. These will all be
    passed in to `go.Scatter`.

    Returns
    -------
    plotly.graph_objs
    '''
    trace = go.Scatter(
      x=list(range(self.r)),
      y=self.impulse,
      name=name,
      **kwargs
    )

    if plot is not None:
      layout = go.Layout(
        title=title,
        xaxis=dict(title='$r$'),
        yaxis=dict(title='${}$'.format(name))
      )
      plot({'data': [trace], 'layout': layout})

    return trace


  #------------------------------------------------------------
  def __repr__(self):
    return repr(self.impulse)

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this transfer function.

    kwargs
    ----------
    name : str or None, default = G
      Prefixes the latex with 'name = ' for whatever name is given. If
      name is None, just gives the equation.
    length : int > 0
      Truncates the impulse response representation to the first `length - 1`
      items, followed by '...', followed by the last item

    Returns
    -------
    latex : str
    '''
    name = kwargs.get('name', 'f(t)')

    # sp.init_printing(use_latex='mathjax')
    if name is not None:
      prefix = '{} = '.format(name)
    else:
      prefix = ''

    if 'length' in kwargs:
      impulse = self.impulse[:kwargs['length'] - 1]
    else:
      impulse = self.impulse

    impulse = ['{:.3f}'.format(fi) for fi in impulse]
    impulsestr = ' & '.join(impulse)

    if 'length' in kwargs:
      impulsestr += r' & \cdots & {:.3f}'.format(self.impulse[-1])

    return r'{}\begin{{bmatrix}}{}\end{{bmatrix}}'.format(prefix, impulsestr)
