#------------------------------------------------------------
# pyrics/Representations/DiscreteGeneralizedStateSpace.py
#
# Representation of a discrete-time genearlized state space model.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

import numpy as np
from sympy import Matrix, eye, latex
from sympy.abc import z

from .DiscreteTime import DiscreteTime


class DTGSS(DiscreteTime):
  '''A discrete time generalized state space model in intricacy-observed form,
  where dynamics are given by:

    x[k + 1] = Ax[k] + Ahat w[k] + Bu[k]
    y[k] = w[k] = Abar x[k] + Atilde w[k] + Bbar u[k]

  where k is a time index, x[k] is in R^n, u[k] is in R^m, and y[k]
  is in R^p.

  Parameters
  ----------
  A      : numpy.array (n x n)
  Ahat   : numpy.array (n x p)
  B      : numpy.array (n x m)
  Abar   : numpy.array (p x n)
  Atilde : numpy.array (p x p)
  Bbar   : numpy.array (p x m)
  '''

  #------------------------------------------------------------
  def __init__(self, A, Ahat, B, Abar, Atilde, Bbar):
    A = np.array(A)
    Ahat = np.array(Ahat)
    B = np.array(B)
    Abar = np.array(Abar)
    Atilde = np.array(Atilde)
    Bbar = np.array(Bbar)

    # Compute Dimensions
    n, n1 = A.shape
    n3, p1 = Ahat.shape
    n2, m = B.shape
    p, n4 = Abar.shape
    p2, p3 = Atilde.shape
    p4, m1 = Bbar.shape

    # Check Dimensions
    if n1 != n:
      raise ValueError(
        'A must be square. Given A as ({} x {})'.format(n, n1)
      )
    if n2 != n:
      raise ValueError((
        'B must have the same number of rows as A. Given A as ' # pylint: disable=W1308
        '({} x {}) but B as ({} x {})'
      ).format(n, n, n2, m))
    if n3 != n:
      raise ValueError((
        'Ahat must have the same number of rows as A. Given A as ' # pylint: disable=W1308
        '({} x {}) but Ahat as ({}, {})'
      ).format(n, n, n3, p1))
    if p1 != p:
      raise ValueError((
        'The number of columns of Ahat must equal the number of rows of Abar. '
        'Given Abar as ({} x {}) but Ahat as ({}, {})'
      ).format(p, n4, n3, p1))
    if n4 != n:
      raise ValueError((
        'Abar must have the same number of columns as A. Given A as ' # pylint: disable=W1308
        '({} x {}) but Abar as ({} x {})'
      ).format(n, n, p, n4))
    if p2 != p1:
      raise ValueError((
        'Atilde must have the same number of rows as Abar. Given Abar as '
        '({} x {}) but Atilde as ({} x {})'
      ).format(p, n4, p2, p3))
    if p3 != p1:
      raise ValueError((
        'Atilde must be square. Given Atilde as ({} x {})'
      ).format(p2, p3))
    if p4 != p:
      raise ValueError((
        'Bbar must have the same number of rows as Abar. Given Abar as '
        '({} x {}) but Bbar as ({} x {})'
      ).format(p, n4, p4, m1))
    if m1 != m:
      raise ValueError((
        'Bbar must have the same number of columns as B. Given B as'
        '({} x {}) but Bbar as ({} x {})'
      ).format(n2, m, p4, m1))

    # Register Matrices and Dimensions
    self.A = A
    self.Ahat = Ahat
    self.B = B
    self.Abar = Abar
    self.Atilde = Atilde
    self.Bbar = Bbar

    self.n = n
    self.m = m
    self.p = p

  #####################################################################
  #   Public Methods
  #####################################################################

  #------------------------------------------------------------
  def simulate(self, Du, x0=None):
    '''Simulate the system with the inputs in Du.

    Parameters
    ----------
    Du : numpy.array (T x m)
      The inputs where each row k is u(k)^T.
    x0 : numpy.array (n x 1)
      The initial conditions. NOT YET IMPLEMENTED.

    Returns
    -------
    Dy : numpy.array (T x p)
      The simulated outputs given the inputs and initial conditions, where
      each row k is y(k)^T.
    '''
    assert x0 is None  # TODO

    A = self.A
    Ahat = self.Ahat
    B = self.B
    Abar = self.Abar
    Atilde = self.Atilde
    Bbar = self.Bbar
    ImAtildeInv = np.linalg.inv(np.eye(Atilde.shape[0]) - Atilde)

    if x0 is None:
      x0 = np.zeros((self.n, 1))

    T, m = Du.shape
    assert m == self.m
    Dy = np.zeros((T, self.p))

    x = x0
    x = x.reshape((self.n, 1))
    for i in range(T):
      u = Du[i, :]
      u = u.reshape((self.m, 1))

      y = ImAtildeInv.dot(Abar.dot(x) + Bbar.dot(u))
      y = y.reshape((self.p, 1))
      x = A.dot(x) + Ahat.dot(y) + B.dot(u)
      x = x.reshape((self.n, 1))
      Dy[i, :] = y.T

    return Dy

  #------------------------------------------------------------
  def is_stable(self, include_marginally=False):
    '''Determine whether this system is stable.

    This system is considered to be stable if its corresponding state space
    model is stable.

    Parameters
    ----------
    include_marginally : bool, default=True
      If True, marginally stable systems are considered to be stable,
      otherwise, marginally stable systems are considered to be unstable.

    Returns
    -------
    is_stable : bool
    '''
    ss = self.to_SS()
    return ss.is_stable(include_marginally)

  #####################################################################
  #   Conversions
  #####################################################################

  #------------------------------------------------------------
  def to_SS(self):
    '''Convert this generalized state space model to a state space model.

    The state space model is found by solving for Atilde in the second equation
    and plugging in the result in to the first equation.

    Returns
    -------
    ss : DTSS
    '''
    from . import DTSS

    Ip = np.eye(self.p)
    inv = np.linalg.inv(Ip - self.Atilde)

    C = inv.dot(self.Abar)
    D = inv.dot(self.Bbar)
    A = self.A + self.Ahat.dot(C)
    B = self.B + self.Ahat.dot(D)

    return DTSS(A, B, C, D)

  #------------------------------------------------------------
  def to_TF(self):
    '''Converts this GSS model into a transfer function matrix.

    Returns
    -------
    G : DTTF
      The discrete time transfer function matrix representation of this system.
    '''
    from . import DTTF

    A = Matrix(self.A)
    Ahat = Matrix(self.Ahat)
    B = Matrix(self.B)
    Abar = Matrix(self.Abar)
    Atilde = Matrix(self.Atilde)
    Bbar = Matrix(self.Bbar)

    In = eye(self.n)
    Ip = eye(self.p)
    L = Atilde + Abar * (z * In - A).inv() * Ahat
    R = Bbar + Abar * (z * In - A).inv() * B
    G = (Ip - L).inv() * R
    return DTTF(G)

  #------------------------------------------------------------
  def to_DSF(self, factor=True):
    '''Convert this state space model into a DSF.

    TODO : Generalize
      FOR NOW ASSUMES THAT Bbar = [I 0]

    Parameters
    ----------
    factor : bool, default=True
      If True, simplifies and factors all rational polynomials (coefficients
      should be rational). Otherwise only simplifies.

    Returns
    -------
    QP : DTDSF
        The discrete time DSF representation of this system.
    '''
    from . import DTTF, DTDSF

    A = Matrix(self.A)
    Ahat = Matrix(self.Ahat)
    B = Matrix(self.B)
    # Abar = Matrix(self.Abar)
    Atilde = Matrix(self.Atilde)
    Bbar = Matrix(self.Bbar)

    p = self.p
    n = self.n
    l = n - p

    A11 = Matrix(A[:p, :p])
    if p < n:
      A12 = Matrix(A[:p, p:n])
      A21 = Matrix(A[p:n, :p])
      A22 = Matrix(A[p:n, p:n])
    else:
      A12 = Matrix([])
      A21 = Matrix([])
      A22 = Matrix([])

    Ahat1 = Ahat[:p, :]
    Ahat2 = Ahat[p:, :]
    B1 = B[:p, :]
    B2 = B[p:, :]

    Il = eye(l)
    Wtilde = A11 + A12 * (z * Il - A22).inv() * A21
    Rtilde = Ahat1 + A12 * (z * Il - A22).inv() * Ahat2
    Vtilde = B1 + A12 * (z * Il - A22).inv() * B2

    What = 1 / z * Wtilde
    Rhat = 1 / z * Rtilde
    Vhat = 1 / z * Vtilde
    # DTTF(What).display(name=r'\hat{W}')

    # Compute the final DNF
    # TODO : create DNF class and convert to DSF using edge abstraction
    Ip = eye(p)
    W = What + Rhat + (Ip - What) * Atilde
    # DTTF(W).display(name='W')
    V = Vhat + (Ip - What) * Bbar

    # Convert to DSF
    Wdiag = W * 0
    for i in range(p):
      Wdiag[i, i] = W[i, i]

    # DTTF((Ip - Wdiag).inv()).display(name='D_W')

    Q = (Ip - Wdiag).inv() * (W - Wdiag)
    P = (Ip - Wdiag).inv() * V

    if factor:
      pass  # TODO use or remove

    return DTDSF(DTTF(Q), DTTF(P))

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    # return repr(self.G)
    return r'A = {}\nAhat = {}\nB={}\nAbar={}\nAtilde={}\nBbar={}'.format(
      repr(self.A), repr(self.Ahat), repr(self.B), repr(self.Abar),
      repr(self.Atilde), repr(self.Bbar)
    )

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this state space model.

    kwargs
    ------
    Mname : str
      The name to give to matrix `M`, where M is any of A, Ahat, B, or Abar,
      Atilde, or Bbar.

    Returns
    -------
    latex : str
    '''
    Aname = kwargs.get('Aname', 'A')
    Ahatname = kwargs.get('Ahatname', r'\hat{A}')
    Bname = kwargs.get('Bname', 'B')
    Abarname = kwargs.get('Abarname', r'\bar{A}')
    Atildename = kwargs.get('Atildename', r'\tilde{A}')
    Bbarname = kwargs.get('Bbarname', r'\bar{B}')


    return (
      r'{} = {} \qquad {} = {} \qquad {} = {}\\\\{} = {} \qquad {} = {}'
      r'\qquad {} = {}'
    ).format(
      Aname, latex(Matrix(self.A)),
      Ahatname, latex(Matrix(self.Ahat)),
      Bname, latex(Matrix(self.B)),
      Abarname, latex(Matrix(self.Abar)),
      Atildename, latex(Matrix(self.Atilde)),
      Bbarname, latex(Matrix(self.Bbar))
    )
