#------------------------------------------------------------
# pyrics/Representations/ConvolutionalTF.py
#
# Representation of a transfer function in convolutional form.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from .SystemRepresentation import SystemRepresentation
from .ConvolutionalScalar import ConvolutionalScalar


class ConvolutionalTF(SystemRepresentation):
  '''Representation of a transfer function matrix in convolutional form.

  Parameters
  ----------
  G : list of lists of ImpulseScalar (p x m array)
  '''

  #------------------------------------------------------------
  def __init__(self, G):
    self.G = G
    self.p = len(G)
    assert self.p > 0
    self.m = len(G[0])

    for inner in self.G:
      assert len(inner) == self.m
      for el in inner:
        assert isinstance(el, ConvolutionalScalar)

  #------------------------------------------------------------
  @property
  def shape(self):
    '''Give the shape of this convlolutional TF, which is p x m.

    Returns
    -------
    shape : (int > 0, int > 0)
    '''
    return (self.p, self.m)

  #####################################################################
  #   Public functions
  #####################################################################

  def scalar_inf_norm(self, r=100):
    '''Compute a matrix of the same dimensions as this, where the entries
    are the infinity-norms of the corresponding convolutional scalars.

    Parameters
    ----------
    r : int > 0, default = 100
      An integer such that every finite impulse response in this matrix is
      approximately zero for all t >= r. See the documentation for
      `ConvolutionalScalar.inf_norm()` for more details.

    Returns
    -------
    M : np.array (p x m)
    '''
    return self.to_impulse(r=r).scalar_one_norm()

  #####################################################################
  #   Conversion functions
  #####################################################################

  #------------------------------------------------------------
  def to_TF(self):
    '''Convert this ConvolutionalTF to a DTTF.

    Returns
    -------
    G : DTTF (p x m)
    '''
    from .DiscreteTransferFunction import DTTF

    G = []
    for i in range(self.p):
      G.append([])
      for j in range(self.m):
        G[i].append(self.G[i][j].to_TF())

    return DTTF(G)

  #------------------------------------------------------------
  def to_impulse(self, r):
    '''Convert this ConvolutionalTF into an ImpulseTF.

    Returns
    -------
    G : ImpulseTF (p x m)
    '''
    from .ImpulseTF import ImpulseTF

    G = []
    for i in range(self.p):
      G.append([])
      for j in range(self.m):
        G[i].append(self.G[i][j].to_impulse(r=r))

    return ImpulseTF(G)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    return repr(self.G)

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this transfer function.

    kwargs
    ----------
    name : str or None, default = G
      Prefixes the latex with 'name = ' for whatever name is given. If
      name is None, just gives the equation.

    Returns
    -------
    latex : str
    '''
    name = kwargs.get('name', 'G(t)')

    # sp.init_printing(use_latex='mathjax')
    if name is not None:
      prefix = '{} = '.format(name)
    else:
      prefix = ''

    matrixstr = '\\\\'.join(
      [' & '.join([el.latex(name=None) for el in inner]) for inner in self.G]
    )
    return '{} \\begin{{bmatrix}}{}\\end{{bmatrix}}'.format(prefix, matrixstr)

  #####################################################################
  #   Magic Functions
  #####################################################################

  #------------------------------------------------------------
  def __getitem__(self, key):
    assert len(key) == 2
    return self.G[key[0]][key[1]]

  #------------------------------------------------------------
  def __setitem__(self, key, value):
    assert len(key) == 2
    self.G[key[0]][key[1]] = value
