#------------------------------------------------------------
# pyrics/Representations/ConvolutionalScalar.py
#
# Representation of a proper rational function in convolutional form.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from sympy.abc import z

from .SystemRepresentation import SystemRepresentation
from .DiscreteTransferFunction import DTTF


class ConvolutionalScalar(SystemRepresentation):
  '''The convolutional representation of a linear impulse response; i.e.,

      f(t) = a delta(t, 0) + sum_{n = 1}^w b_n c_n^t

  for parameters a, b_n, and c_n, n = 1, ..., w.

  Parameters
  ----------
  params : list (lengh 2 * w + 1)
    [a, b_1, c_1, b_2, c_2, ..., b_w, c_w].
  tol : number > 0
    If either |b_i| < tol or |c_i| < tol, that parameter pair is removed from
    the list (this helps clean up the conversions).
  '''

  #------------------------------------------------------------
  def __init__(self, params, tol=1e-6):

    if len(params) % 2 != 1:
      raise ValueError(
        'params must have an odd number of entries. Contains {} entries'.format(
          len(params)
        )
      )

    rparams = [params[0]]
    rest = params[1:]
    for i in range(0, len(rest), 2):
      b = rest[i]
      c = rest[i + 1]
      if abs(b) < tol or abs(c) < tol:
        continue
      rparams += [b, c]

    params = rparams

    self.w = int((len(params) - 1) / 2)
    self.params = params

  #####################################################################
  #   Public functions
  #####################################################################

  def inf_norm(self, r=100):
    '''Compute the infinity norm of this convolutional transfer function.

    The infinity norm is the one norm of the corresponding impulse response.

    r : int > 0
      See parameter `r` in `to_impulse()`. The norm must convert to the finite
      impulse response before computing the norm. In the future, an option will
      be added to automatically determine r, but for now, it must predefined
      (defaults to 100).

    Returns
    -------
    norm : float >= 0
    '''
    imp = self.to_impulse(r=r)
    return imp.one_norm()

  #####################################################################
  #   Conversion functions
  #####################################################################

  #------------------------------------------------------------
  def to_TF(self):
    '''Converts this convolutional form into a SISO transfer function.

    Returns
    -------
    tf : DiscreteTransferFunction (1x1)
    '''
    # a = self.params[0]
    rest = self.params[1:]
    bc = [
      (rest[i], rest[i + 1]) for i in range(0, len(rest), 2)
    ]

    G = 0
    for bi, ci in bc:
      alpha = bi * ci
      beta = ci
      G += alpha / (z - beta)

    return DTTF([[G]])

  #------------------------------------------------------------
  def to_impulse(self, r=100):
    '''Converts this convolutional form into an impulse response.

    Parameters
    ----------
    r : int > 0
      Creates an impulse response g(t) = [f(0), ..., f(r - 1)], where g(t) is
      the impulse response and f(t) is the convolutional form. Note that f(t)
      should be approximately zero for every t >= r; however, this condition is
      not checked.

      TODO - auto find a good r

    Returns
    -------
    ConvolutionalScalar
    '''
    from .ImpulseScalar import ImpulseScalar

    a = self.params[0]
    rest = self.params[1:]

    def _f(t):

      if t == 0:
        rs = a
      else:
        rs = 0

      for i in range(0, len(rest), 2):
        b = rest[i]
        c = rest[i + 1]
        rs += b * c ** t

      return rs

    g = []
    for t in range(r):
      g.append(_f(t))

    return ImpulseScalar(g)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    return repr(self.params)

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this transfer function.

    kwargs
    ----------
    name : str or None, default = G
      Prefixes the latex with 'name = ' for whatever name is given. If
      name is None, just gives the equation.

    Returns
    -------
    latex : str
    '''
    name = kwargs.get('name', 'f(t)')

    # sp.init_printing(use_latex='mathjax')
    if name is not None:
      prefix = '{} = '.format(name)
    else:
      prefix = ''

    if len(self.params) == 1:
      if self.params[0] == 0:
        return '{} 0'.format(prefix)
      return '{} {:.3f}\\delta_{{(t, 0)}}'.format(prefix, self.params[0])

    a = self.params[0]
    rest = self.params[1:]
    bc = [
      '{:.3f}({:.3f})^t'.format(rest[i], rest[i + 1])
      for i in range(0, len(rest), 2)
    ]
    bcstr = ' + '.join(bc)

    return '{} {:.3f}\\delta_{{(t, 0)}} + {}'.format(prefix, a, bcstr)
