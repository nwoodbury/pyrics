#------------------------------------------------------------
# pyrics/Representations/DiscreteTransferFunction.py
#
# Representation of a discrete-time transfer function.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

import warnings
from copy import copy, deepcopy

import numpy as np
# import sympy as sp
from sympy import (
  oo, limit, latex, factor, nsimplify, apart, fraction, degree, Poly
)
from sympy.abc import z
from sympy.matrices import Matrix
from sympy.core.add import Add
from pyrics.PolynomialUtilities import (
    is_proper, is_rational_polynomial
)

from pyrics.Algorithms import frequencyReconstruct
from .DiscreteTime import DiscreteTime


class DTTF(DiscreteTime):
  '''Representation of a discrete time transfer function.

  Parameters
  ----------
  G : sympy rational polynomial in z or sympy Matrix of rational polynomials.
  '''

  #------------------------------------------------------------
  def __init__(self, G):
    if not isinstance(G, Matrix):
      G = Matrix(G)
    if len(G.shape) != 2:
      raise ValueError(
        'G must be a two-dimensional matrix'
      )

    # TODO : ensure that only symbol is z

    self.p, self.m = G.shape
    for i in range(self.p):
      for j in range(self.m):

        G[i, j] = nsimplify(G[i, j])
        G[i, j] = factor(G[i, j])

        if not is_rational_polynomial(G[i, j], z):
          raise ValueError(
            'G[{},{}] must be a rational polynomial in z, given {}'.format(
              i, j, G[i, j]
            )
          )

        if not is_proper(G[i, j], z):
          warnings.warn(
            'G[{},{}] = {} is not proper'.format(
              i, j, G[i, j]
            )
          )
    self.G = G

  #####################################################################
  #   Public Properties
  #####################################################################

  #------------------------------------------------------------
  @property
  def shape(self):
    '''The dimensions of this transfer function.

    Returns
    -------
    m : int > 0
      The number of rows in the transfer function.
    n : int > 0
      The number of columns in the transfer function.
    '''
    return self.G.shape

  #####################################################################
  #   Public Methods
  #####################################################################

  #------------------------------------------------------------
  def reconstruct(self, K=None):
    '''Reconstruct the network (DSF) generating this transfer function.

    Methodology described in [1].

    Parameters
    ----------
    K : np.array (p^2 + pm x pm) or None, default=None
      The identifiability conditions required to map the TF to the DSF uniquely.
      If None, assumes that P is diagonal (target specificity); however, an
      exception is raised in this case if p != m.

    Returns
    -------
    F : DTDSF
      The reconstructed network.

    Sources
    -------
    [1] J. Gonçalves and S. Warnick, "Necessary and Sufficient Conditions
        for Dynamical Structure Reconstruction of LTI Networks," IEEE
        Transactions on Automatic Control, Aug. 2008.
    '''
    from . import DTDSF

    Q, P = frequencyReconstruct(self.G, K)
    return DTDSF(DTTF(Q), DTTF(P))

  #------------------------------------------------------------
  def limit(self, approaches=oo):
    '''Compute the limit of this transfer function as z approaches `approaches`.

    Parameters
    ----------
    approaches : number or sympy.oo (infinity), default=infinity

    Returns
    -------
    limit : numpy.array (p x m)
    '''
    lim = np.zeros((self.p, self.m))

    for i in range(self.p):
      for j in range(self.m):
        lim[i, j] = limit(self.G[i, j], z, approaches)

    return lim

  #------------------------------------------------------------
  def inv(self):
    '''Compute the inverse of this transfer function.

    Returns
    -------
    inv : DTTF
      The inverse.
    '''
    return DTTF(self.G.inv())

  #------------------------------------------------------------
  def diag(self):
    '''Return a copy of this transfer function where every entry but the
    diagonals have been set to 0.

    NOTE: Only works on square transfer functions.

    Returns
    -------
    Gdiag : DTTF
      A TF with the same dimensions as this transfer function.
    '''
    m, n = self.shape
    if m != n:
      raise Exception('Can only find diag of square transfer functions')

    Gdiag = self.G * 0
    for i in range(m):
      Gdiag[i, i] = self.G[i, i]

    return DTTF(Gdiag)

  #------------------------------------------------------------
  def scalar_inf_norm(self, r=100):
    '''Compute a matrix of the same dimensions as this, where the entries
    are the infinity-norms of the corresponding transfer function scalars.

    Parameters
    ----------
    r : int > 0, default = 100
      An integer such that every finite impulse response in this matrix is
      approximately zero for all t >= r. See the documentation for
      `ConvolutionalScalar.inf_norm()` for more details.

    Returns
    -------
    M : np.array (p x m)
    '''
    return self.to_convolutional().to_impulse(r=r).scalar_one_norm()

  #------------------------------------------------------------
  @property
  def T(self):
    '''Compute and return the transpose of this DTTF.

    Returns:
    --------
    GT : DTTF (m x p)
    '''
    return DTTF(self.G.T)

  #------------------------------------------------------------
  def copy(self):
    return copy(self)

  #------------------------------------------------------------
  def deepcopy(self):
    return deepcopy(self)

  #####################################################################
  #   Conversion functions
  #####################################################################

  #------------------------------------------------------------
  def to_convolutional(self):
    '''Converts this DTTF to a convolutional form.

    Returns
    -------
    Gconv : ConvolutionalTF
    '''
    from .ConvolutionalScalar import ConvolutionalScalar
    from .ConvolutionalTF import ConvolutionalTF

    D = self.limit()

    Gconv = []
    for i in range(self.p):
      Gconv.append([])
      for j in range(self.m):
        tf = self.G[i, j]
        d = D[i, j]
        tf -= d
        ap = apart(tf)

        if isinstance(ap, Add):
          args = ap.args
        else:
          args = [ap]

        params = []
        bsum = 0

        for arg in args:
          if arg == 0:
            continue
          num, den = fraction(arg)
          assert degree(num) == 0
          assert degree(den) == 1
          alpha = float(num)
          scale, beta = Poly(den).coeffs()
          beta = -1 * float(beta)
          scale = float(scale)
          alpha /= scale
          beta /= scale
          b = alpha / beta
          c = beta
          params.append(b)
          params.append(c)
          bsum += b

        params = [-bsum + d] + params
        ci = ConvolutionalScalar(params)
        Gconv[i].append(ci)

    return ConvolutionalTF(Gconv)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    return repr(self.G)

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this transfer function.

    kwargs
    ----------
    name : str or None, default = G
      Prefixes the latex with 'name = ' for whatever name is given. If
      name is None, just gives the matrix.

    Returns
    -------
    latex : str
    '''
    name = kwargs.get('name', 'G')

    # sp.init_printing(use_latex='mathjax')
    if name is not None:
      prefix = '{} = '.format(name)
    else:
      prefix = ''

    return '{}{}'.format(prefix, latex(self.G))

  #####################################################################
  #   Magic Functions
  #####################################################################

  #------------------------------------------------------------
  def __neg__(self):
    return DTTF(-1 * self.G)

  #------------------------------------------------------------
  def __add__(self, other):
    m, n = self.shape
    m1, n1 = self.shape

    if m != m1 or n != n1:
      raise ValueError(
        'Dimension mismatch, trying to add transfer functions of '
        'shapes ({}, {}) and ({}, {})'.format(
          m, n, m1, n1
        )
      )

    return DTTF(self.G + other.G)

  #------------------------------------------------------------
  def __radd__(self, other):
    return self.__add__(other)

  #------------------------------------------------------------
  def __rmul__(self, other):
    return other.__mul__(self)

  #------------------------------------------------------------
  def __sub__(self, other):
    return self.__add__(-other)

  #------------------------------------------------------------
  def __mul__(self, other):
    m, n = self.shape
    m1, n1 = other.shape

    if n != m1:
      raise ValueError(
        'Dimension mismatch, trying to multiply transfer functions of '
        'shapes ({}, {}) and ({}, {})'.format(
          m, n, m1, n1
        )
      )

    return DTTF(self.G * other.G)

  #------------------------------------------------------------
  def __pow__(self, n):
    if not isinstance(n, int):
      raise ValueError(
        'Can only raise transfer functions to integer powers'
      )

    if n < 0:
      G = self.G.inv()
      n *= -1
    else:
      G = self.G

    return DTTF(G ** n)

  #------------------------------------------------------------
  def __getitem__(self, key):
    return DTTF(self.G[key])

  #------------------------------------------------------------
  def __setitem__(self, key, value):
    self.G[key] = value

  #------------------------------------------------------------
  def __copy__(self):
    return DTTF(self.G)

  #------------------------------------------------------------
  def __deepcopy__(self, memodict={}):  #pylint: disable=W0102
    return DTTF(deepcopy(self.G))
